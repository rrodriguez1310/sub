
exports.verificaToken = (req, res, next) => {

  const token = req.headers['x-user-browser']
  const token2 = req.headers['x-user-os']

  if (token == undefined || token2 == undefined || token.trim() == ''  || token2.trim() == '') {


      return res.status(400).json({ message: "token no valido" })
    }


  
  if (req.body.rut == undefined || req.body.email == undefined || req.body.telefono == undefined || req.body.salario == undefined || req.body == undefined || req.body.length <= 2) {

    return res.status(400).json({ message: "Argumentos no validos" })
  }
  if ((req.body.rut).trim() == '' || (req.body.email).trim() == '' || (req.body.telefono).toString().trim() == '' || (req.body.salario).toString().trim() == '') {

    return res.status(400).json({ message: "Falta alguno de los argumentos" })
  }


  next();

}



