const express = require("express");
var bodyParser = require('body-parser')
const app = express();

var controller = require('./controllers/index');

//middleware
const Autenticacion = require('./middleware/index');



app.use(express.json());
//coors
 app.all('*',  (req, res, next) =>{
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Credentials', true);
    res.set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.set('Access-Control-Allow-Headers', 'X-Requested-With, x-user-browser, x-user-os, Content-Type, Authorization');
    res.setHeader('Content-Type', 'application/json')
    if ('OPTIONS' == req.method) return res.status(200).send();
    next();
}); 



var apiRoutes = express.Router();


app.use(apiRoutes);

app.post('/', function (req, res) {
    res.status(200).json(":)");
});







apiRoutes.use(Autenticacion.verificaToken)
//apiRoutes.use(Autenticacion.verificaCampos)

apiRoutes.route('/endpoint').post(controller.endpooint);

//rutas protegidas------






app.use('/api', apiRoutes);

//start-stop server
app.listen(4000, () => {
    console.log("El servidor está inicializado en el puerto 4000");
});


