

const initialState = {
    rut: '',
    telefono: '',
    email:''

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'modifica':
           
              return {...state, 
                rut: action.payload.rut,
                telefono: action.payload.telefono,
                email: action.payload.email
            }
             
        
        default:
            return state
    }
}

export default reducer;