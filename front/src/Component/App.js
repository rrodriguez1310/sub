

import { AppRouter } from '../routers/Approuter';
import { store } from '../store/store';
import { Provider } from 'react-redux';



export const App = () => {
  return (
    <Provider store={store}>

        <AppRouter />

    </Provider>
  )
}
