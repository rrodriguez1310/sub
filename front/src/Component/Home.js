//import logo from './logo.svg';
//import './App.css';
import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import FormIzq from './App/Form';
import NavBar from './Nav';
import CardIzquierda from './App/CardIzq';


import '../index.css'


function Home() {



  return (

    <div className="App">
      <div className="Nav">
        <NavBar />
      </div>
      <div className="bg" >
        <Container fluid>
          <Row>
            <Col sm={6}>
              <Row>
                <h1 className='letteresup'>
                  Tu Cuenta Corriente <br />
                  <strong>100% online</strong>
                </h1>
              </Row>
              <Row className="d-none d-lg-block">
                <div className='cardIzqcss'>
                  <CardIzquierda />
                </div>
              </Row>
            </Col>
            <Col sm={6}>
              <div className='separacionform'>
                <FormIzq />
              </div>

            </Col>
          </Row>
        </Container>

      </div>


    </div>

  );
}

export default Home;
