import { Nav, Navbar, Container, Offcanvas } from 'react-bootstrap';
import logo from '../assets/logo-banco-falabella.png';


import '../index.css'

const NavBar = () => {
  return (

    <Navbar bg="light" expand={false}>
      <Container fluid>
        <div >
          <Navbar.Toggle aria-controls="offcanvasNavbar" />
        </div>
        <Navbar.Brand className="navbar-brand-custom" ><img width="95" height="30" className='img-responsive' src={logo} alt='' /></Navbar.Brand>
        <Navbar.Offcanvas
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
          placement="start"
        >
          <Offcanvas.Header closeButton>
            <Offcanvas.Title id="offcanvasNavbarLabel">Falabella</Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body>
            <Nav className="justify-content-end flex-grow-1 pe-3">
            <Nav.Link href="http://saga-falabella.com/falabella-pe/">Saga Falabella</Nav.Link>
                            <Nav.Link href="https://www.viajesfalabella.cl/">Viajes Falabella</Nav.Link>
            </Nav>

          </Offcanvas.Body>
        </Navbar.Offcanvas>

      </Container>
    </Navbar>
  );
}

export default NavBar;
