import React from 'react';
import { Nav, Navbar, Container } from 'react-bootstrap';


export const NavBarBlack = () => {
    return (
        <div className='menuBlack'>
            <Navbar collapseOnSelect expand="lg" bg="dark " variant="dark">
                <Container>
                    <Navbar.Brand href="#home"></Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="http://saga-falabella.com/falabella-pe/">Saga Falabella</Nav.Link>
                            <Nav.Link href="https://www.viajesfalabella.cl/">Viajes Falabella</Nav.Link>
                            <Nav.Link href="https://web.segurosfalabella.com/cl/">Seguros Falabella</Nav.Link>
                            <Nav.Link href="https://www.sodimac.cl/sodimac-cl/">Sodimac</Nav.Link>
                            <Nav.Link href="https://www.tottus.cl/">Tottus</Nav.Link>
                            <Nav.Link href="https://www.falabella.com/falabella-cl/collection/ofertas-mujer-ropa-v2?isPLP=1">Maestro</Nav.Link>
                        </Nav>
                        <Nav>
                            <Nav.Link href="#pricing"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                                <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                            </svg></Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}
