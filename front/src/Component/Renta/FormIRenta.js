
import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { useForm } from '../CustomHook/useForm';
import { useSelector } from 'react-redux';
import { saveAs } from 'file-saver';
import { useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2';
import '../../index.css'

var platform = require('platform');


const FormRenta = () => {
    const navigate = useNavigate();

    const { rut, telefono, email } = useSelector(state => state.reducer)

    const [formValues, handleInputChange] = useForm({
        salario: ''

    });

    const { salario } = formValues;

    const handleRegister = async (e) => {
        e.preventDefault();
        let browser = platform.name + ' ' + platform.version;
        let SO = platform.os.family + ' ' + platform.os.architecture;

        const requestOptions = {
            method: 'POST',
            headers: { 'x-user-browser': browser, 'x-user-os': SO, 'Content-Type': 'application/json' },
            body: JSON.stringify({ rut, telefono, email, salario })
        };


        fetch('http://localhost:4000/api/endpoint', requestOptions).then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Something went wrong ...');
            }
        })
            .then(data => {
                const blob = new Blob(['Rut: ', rut, ',', '\n', 'Teléfono: ', telefono, ',', '\n', 'Email: ', email, ',', '\n', 'Salario: ', salario], { type: 'text/plain;charset=utf-8' });
                saveAs(blob, 'falabella.txt');
                Swal.fire(
                    'Excelente Trabajo!',
                    'Los datos fueron cargados correctamente.',
                    'success'
                )
               
            }, navigate('/'))
            .catch(error => Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Se consiguio un error en la carga de datos!',
               
            }),
            navigate('/'));

    }


    return (
        <Form className='form-izqRenta' onSubmit={handleRegister}>
            <div className='padding'>

                <div className='fg'><font color="green">Ayúdanos</font> con unos<br /> datos más</div>
                <div className="fgrayRenta">
                    Porque queremos dar una oferta hecha a la<br /> medida para ti. Necesitamos saber un poco más.
                </div>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <div className='label-imp'>¿Cuál es tu renta liquida/ingreso mensual?:</div>
                    <Form.Control name="salario" size="sm" type="number" placeholder="Ej: $400.000" value={salario} onChange={handleInputChange} />

                </Form.Group>


                <Button disabled={!salario} className='buttonform' size="lg" type="submit">
                    Continuar
                </Button>
            </div>
        </Form>


    )
}
export default FormRenta;