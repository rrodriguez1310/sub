import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import FormRenta from './Renta/FormIRenta';
import NavBar from './Nav';
import { NavBarBlack } from './Renta/NavBarBlack';



export const Renta = () => {
    return (
        <div>
            <NavBarBlack />
            <div className="Nav">
                <NavBar />
            </div>
            <div className="bgRenta" >
                <Container fluid>
                    <Row>
                        <Col sm={6}>

                        </Col>
                        <Col sm={6}>
                            <div className='separacionform'>
                                <FormRenta />
                            </div>

                        </Col>
                    </Row>
                </Container>

            </div>


        </div>

    )
}
