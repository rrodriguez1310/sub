import React from 'react';
import { Col, Row, Image, Container, Card } from 'react-bootstrap';
import logo1 from '../../assets/ico-cmr.png';
import logo2 from '../../assets/opu.png';
import logo3 from '../../assets/visa-blue-png-2.png'



function cardIzquierda() {
    return (
        <div>
            <Card className='cardIzqcss  opacity-50'>
                <Card.Body>
                        <Container>
                            <Row>
                            <Col ></Col>
                                <Col xs={1}><Image src={ logo1} height={40} width={40}/></Col>
                                <Col xs={2}><div className='pcard'>Acumula<br/>CRM puntos</div></Col>
                                <Col xs={1}><Image src={ logo2} height={40} width={40}/></Col>
                                <Col xs={2}><div className='pcard'>Descuentos<br/>exclusivos</div></Col>
                                <Col xs={1} className='imargeniaz' ><Image src={ logo3} height={15} width={30}/></Col>
                                <Col xs={3}><div className='pcard'>Compra<br/>en todo el mundo</div></Col>
                            </Row>
                        </Container>
           

                </Card.Body>
            </Card>
        </div>
    )
}

export default cardIzquierda
