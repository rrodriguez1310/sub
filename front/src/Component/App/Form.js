import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { useForm } from '../CustomHook/useForm';
import {  useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionscreator } from '../../store/index';
import {  useNavigate } from 'react-router-dom'








import '../../index.css'



const FormIzq = () => {

    const navigate = useNavigate();
 //   const state = useSelector(state => state.initial);
    const dispatch = useDispatch();
    const { modificaInicial} = bindActionCreators(actionscreator, dispatch);
   // console.log(AC)

    const [formValues, handleInputChange] = useForm({
        rut: "",
        telefono: "",
        email: ""
    });

    const { rut, email, telefono } = formValues;

    const handleRegister = async(e) => {
        e.preventDefault();
        console.log(rut, email, telefono)
        modificaInicial({ rut, email, telefono })

        navigate('/renta');

    
    
        
        

    }
    

    return (
        <Form className='form-izq' onSubmit={ handleRegister }>
            <div className='padding'>
          
                    <div className='fg'><font color="green">Ingresa</font> tus datos</div>
                <div className="fgray">
                    Solo necesitas tener tu cédula para realizar la solicitud.
                </div>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <div className='label-imp'>Rut:</div>
                    <Form.Control name="rut" size="sm" type="text" placeholder="Ej: 11.111.111-1" value={rut}   onChange={ handleInputChange }/>
                    <div className='label-imp'>Celular:</div>
                    <Form.Control name="telefono" size="sm" type="number" placeholder="Ej: 987654321" value={telefono}   onChange={ handleInputChange }/>
                    <div className='label-imp'>Correo Electronico:</div>
                    <Form.Control name="email" size="sm" type="email" placeholder="Ej: correo@correo.cl" value={email}   onChange={ handleInputChange }/>
                </Form.Group>


                <Button className="buttonform" disabled={!rut||!telefono ||!email}  type="submit">
                    Continuar
                </Button>
            </div>
        </Form>


    )
}
export default FormIzq;