
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import  Home  from '../Component/Home';
import { Renta } from '../Component/Renta';






export const AppRouter = () => {
    return (
        <>

            <div >
                <BrowserRouter>
                    <Routes>
                        <Route exact path="/" element={<Home/>} />
                        <Route exact path="/renta" element={<Renta />} />

                    </Routes>
                </BrowserRouter>
            </div>
        </>
    )
}



